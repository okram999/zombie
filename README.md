zombie Cookbook
===============
This is a wrapper cookbook that install the oracle jdk 8


Requirements
------------
run 'berks install' to resolve the dependencies from the berksfile

Attributes
----------
Certain Attributes have been preset to install the oracle java 8



License and Authors
-------------------
Authors: niristotle okram
