default['java']['jdk_version'] = '8'
default['java']['accept_license_agreement'] = true
default['java']['install_flavor'] = 'oracle'
default['java']['oracle']['accept_oracle_download_terms'] = true
override['java']['java_home'] = '/opt/mount1'
