name             'zombie'
maintainer       'niristotle okram'
maintainer_email 'nirish.okram@gmail.com'
license          'All rights reserved'
description      'Installs/Configures zombie'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.6'

depends 'java'
